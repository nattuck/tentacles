#!/bin/bash

perl Makefile.PL
make
sudo checkinstall -D -y --pkgname tentacle-perl --pkglicense GPL \
     --pkgversion 0.1 --maintainer "Nat Tuck" \
     --requires perl \
     --install=0 --backup=0
sudo ./clean.sh
sudo chown $USER *.deb

if [[ -e Makefile ]]; then
    sudo make distclean
fi

sudo rm -rf backup-*-tentacle-perl.tgz doc-pak description-pak

