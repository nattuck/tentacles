package Tentacle::ShellResult;

sub new {
    my ($class) = @_;
    my $self = {
        status => -1,
        out => '',
        err => '',
    };
    return bless($self, $class);
}

1;
