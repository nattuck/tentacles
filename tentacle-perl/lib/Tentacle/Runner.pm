package Tentacle::Runner;
use 5.20.0;

require Exporter;
our @ISA = qw(Exporter);

our @EXPORT = qw(hello);

sub hello {
    say "Hello";
}


1;

