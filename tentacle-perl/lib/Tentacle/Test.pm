package Tentacle::Test;
use 5.20.0;
use warnings 'FATAL';

use English qw($EUID);
use IO::Select;
use IO::Handle;
use Data::Dumper;
use File::Temp ();
use IPC::Open3;
use Term::ANSIColor qw(color);
use Symbol qw(gensym);
use JSON;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(test report shell dump_json);

our $TIMEOUT   = 10;
our $LIMIT_MEM = 128; # MB
our $LIMIT_FDS = 128;

our @RESULTS = ();

sub test {
    my ($name, $avail, $code) = @_;
    my $score = 0;

    pipe(my $rd, my $wr);

    say(color('bold blue') . "test: $name..." . color('reset'));

    if ((my $cpid = fork())) {
        waitpid($cpid, 0);

        my $sel = IO::Select->new($rd);
        my ($rd1) = $sel->can_read($TIMEOUT);
        if ($rd1) {
            my $temp;
            sysread($rd1, $temp, 4);
            ($score) = unpack("l", $temp);
        }
        else {
            $score = 0;
        }
    }
    else {
        $EUID = 1000 if ($EUID == 0);
        my $res = $code->();
        my $temp = pack("l", $res);
        syswrite($wr, $temp, 4);
        exit(0);
    }

    push @RESULTS, {
        name => $name,
        score => $score,
        avail => $avail,
    };

    say(color('bold blue') . "  ...$name score: $score/$avail" . color('reset'));
}

sub shell {
    my ($cmd) = @_;
    my $out_text = "";
    my $err_text = "";
    (\*STDOUT)->flush();
    (\*STDERR)->flush();
    local $| = 1;

    my $max_kb = $LIMIT_MEM * 1024;

    my $temp = File::Temp->new();
    $temp->say("#!/bin/bash");
    $temp->say("ulimit -m $max_kb -n $LIMIT_FDS");
    $temp->say("timeout -s KILL $TIMEOUT $cmd");
    $temp->flush();

    my $tname = $temp->filename();
    my ($cin, $cout, $cerr) = (gensym, gensym, gensym);
    my $cpid = open3($cin, $cout, $cerr, "bash $tname");

    my $sel = IO::Select->new($cout, $cerr);
    while ($sel->handles()) {
        for my $hh ($sel->can_read()) {
            my $buf;
            my $nn = sysread($hh, $buf, 256);
            if ($nn > 0) {
                if ($hh == $cout) {
                    $out_text .= $buf;
                    syswrite(\*STDOUT, $buf, $nn);
                }
                if ($hh == $cerr) {
                    $err_text .= $buf;
                    syswrite(\*STDOUT, color('bold red'));
                    syswrite(\*STDOUT, $buf, $nn);
                    syswrite(\*STDOUT, color('reset'));
                }
            }
            else {
                $sel->remove($hh);
            }
        }
    }

    waitpid($cpid, 0);
    my $status = $?;
    my $code   = $status >> 8;

    return {
        status => $status,
        code   => $code,
        out    => $out_text,
        err    => $err_text,
    };
}

sub score_total {
    my $total_score = 0;
    my $total_avail = 0;
    for my $res (@RESULTS) {
        $total_score += $res->{score};
        $total_avail += $res->{avail};
    }
    return ($total_score, $total_avail);
}

sub dump_json {
    my ($secret) = @_;
    $secret ||= "JSON_DUMP";

    my ($ts, $ta) = score_total();
    my $data = {
        tests => \@RESULTS,
        total => {
            score => $ts,
            avail => $ta,
        }
    };

    say "\n$secret";
    say encode_json($data);
    say "$secret\n";
}

sub report {
    say(color("bold cyan")."Testing report:".color("reset"));
    for my $res (@RESULTS) {
        my $name = $res->{name};
        my $score = $res->{score};
        my $avail = $res->{avail};
        say "test $name: $score/$avail";
    }
    my ($ts, $ta) = score_total();
    say "Total: $ts/$ta";
}

1;
