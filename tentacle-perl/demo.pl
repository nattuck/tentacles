#!/usr/bin/perl
use 5.20.0;
use warnings 'FATAL';

use lib 'lib';
use Data::Dumper;

use Tentacle::Test;
$Tentacle::Test::TIMEOUT = 2;

test "demo 1", 1, sub {
    return 1;
};

test "demo 2", 2, sub {
    system("whoami");
    die "test crashed";
};

test "demo 3", 3, sub {
    my $res = shell "perl hello.pl";
    return 2;
};

dump_json();
report();
