
# Tentacle: Sandboxed Testing Support Library

```

test "foo", sub {
  return 1;
};


```

The test code is run in a separate process.

